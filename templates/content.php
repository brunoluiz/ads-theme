<?php 
$simfolio = new Simfolio();
$id = get_the_ID();
$description = $simfolio->get_description($id);
$photos      = $simfolio->get_photos($id);
$main_photo  = $simfolio->get_main_photo($id);

if ($main_photo == NULL) {
    return false;
}

$ratio = $main_photo['width']/$main_photo['height'];
$url = $main_photo['sizes']['large']['url'];

// $random = (float)rand()/(float)getrandmax();
// $w = (1 +  2 * $random << 0 ) * 200;
// if ($w > 500) $w = 500;
$w = 400;
$h = $w;

// Colours using random RGB colours
// $colours = [
//   "rgb(243, 156, 18)",
//   "rgb(211, 84, 0)",
//   "rgb(0, 106, 63)",
//   "rgb(41, 128, 185)",
//   "rgb(192, 57, 43)",
//   "rgb(135, 0, 0)",
//   "rgb(39, 174, 96)"
// ];

// Colours using compound colours
// $colours = [
//   '#2E44CC',
//   '#6F7599',
//   '#54C5FF',
//   '#FFC093',
//   '#CC582E'
// ];

$colours = [
  '#000',
];

$random_color = rand(0,count($colours)-1);
?>
<div class='item'
  style='width: <?=$w?>px; 
  height: <?=$h?>px; 
  background-color: <?=$colours[$random_color]?>' >
  <a class='item-wrapper' 
     href="<?=get_permalink($id)?>"
     style='background-image: url("<?=$url?>");' ></a>
  <h2 class="item-title"><?php the_title(); ?></h2>
</div>