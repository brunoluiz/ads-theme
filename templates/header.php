<?php
    $addional_tag = false;
    // if ( is_home() || is_front_page() || is_archive() )
    // {
    //     $addional_tag = true;
    // }

    $items_no_dropdown_limit = 3;
?>
<?php 
    $navbarClass = 'navbar navbar-default navbar-fixed-top';
    if($addional_tag)
        $navbarClass = 'navbar navbar-default navbar-fixed-top navbar-border';
?>
<div class="<?php echo $navbarClass; ?>">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>" >
                <img 
                id='logo' 
                alt='<?php bloginfo("name"); ?>' 
                src='<?= get_template_directory_uri(); ?>/dist/images/logo.png' 
                class="img-responsive" style='height: 100%'
                />
            </a>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav">
                <?php $terms = get_terms('skill'); ?>
                
                <?php // Create dropdown only if there is too much "skills" categories ?>
                <?php if (count($terms) > $items_no_dropdown_limit): ?>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Projetos <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                <?php endif; ?>

                <?php
                foreach ( $terms as $term ):
                    // The $term is an object, so we don't need to specify the $taxonomy.
                    $term_link = get_term_link( $term );

                    // If there was an error, continue to the next term.
                    if ( is_wp_error( $term_link ) ) {
                        continue;
                    }
                    // We successfully got a link. Print it out.
                ?>
                    <li><a href="<?=esc_url( $term_link )?>"><?=$term->name?></a></li>
                <?php endforeach; ?>
                    <?php // Insert an option to see all projects ?>
                    <li><a href="<?=get_home_url()?>">Todos Projetos</a></li>

                <?php // Close dropdown menu <ul> tag ?>
                <?php if (count($terms) > $items_no_dropdown_limit): ?>
                  </ul>
                </li>
                <?php endif; ?>
                
                <li class="divider-vertical"></li>
                
                <?php 
                // List pages
                $pages = get_pages();
                foreach ($pages as $page):
                ?> 
                <li><a href="<?=get_page_link($page->ID)?>"><?=$page->post_title?></a></li>
                <?php endforeach; ?>
            </ul>
            <ul class="nav navbar-nav navbar-right" id="social-nav">
                <li>
                    <a href="https://www.instagram.com/andrediogoo" class='social-nav-instagram' target='_blank'>
                        <i class="fa fa-instagram"></i> <span class='social-nav-title'>Instagram</span>
                    </a>
                </li>
                <li>
                    <a href="https://www.youtube.com/channel/UCnlBrh-C57IoHYG84ScgIUA" class='social-nav-youtube' target='_blank'>
                        <i class="fa fa-youtube-square"></i> <span class='social-nav-title'>YouTube</span>
                    </a>
                </li>
                <li>
                    <a href="https://www.pinterest.com/andreediogo/" class='social-nav-pinterest' target='_blank'>
                        <i class="fa fa-pinterest-square"></i> <span class='social-nav-title'>Pinterest</span>
                    </a>
                </li>
                <li>
                    <a href="mailto:andrediogo2004@gmail.com" class='social-nav-email' target='_blank'>
                        <i class="fa fa-envelope-o"></i> <span class='social-nav-title'>E-mail</span>
                    </a>
                </li>
            </ul>
        </div>

    </div>
</div>