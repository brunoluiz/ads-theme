<?php while (have_posts()) : the_post();
  $id = get_the_ID();
  
  $simfolio = new Simfolio();
  $description = $simfolio->get_description($id);
  $photos      = $simfolio->get_photos($id);
  $main_photo  = $simfolio->get_main_photo($id);

  if ($main_photo == NULL) {
    continue;
  }
?>
<article <?php post_class(); ?> id="project-content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <figure id="project-photo" >
          <img src="<?=$main_photo['sizes']['large']['url']?>" />
          <figcaption>
            <!-- <span class='project-photo-title'><?=$main_photo['title']?></span>  -->
            <!-- <span class='project-photo-caption'><?=$main_photo['caption']?></span> -->
          </figcaption>
          <!-- <a class="project-photo-viewer" href="<?=$main_photo['sizes']['large']['url']?>">(Visualizar foto em alta-resolução)</a> -->
        </figure>
      </div>
      <div class="col-md-4">
        <h2>//<?php the_title(); ?></h2>
        <p><?=$description?></p>
        <div id="project-thumbs" class="photoswipe-collection" itemscope itemtype="http://schema.org/ImageGallery">
          <?php foreach ($photos as $photo): ?>
          <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a  href="<?=$photo['sizes']['large']['url']?>" 
                itemprop="contentUrl"
                data-height="<?=$photo['sizes']['large']['height']?>"
                data-width="<?=$photo['sizes']['large']['width']?>" 
                alt="<?=$photo['title']?>" >
              <img 
                src="<?=$photo['sizes']['thumbnail']['url']?>" 
                data-src="<?=$photo['sizes']['large']['url']?>" 
                data-src-full="<?=$photo['sizes']['full']['url']?>" 
                data-title="<?=$photo['title']?>" 
                data-caption="<?=$photo['caption']?>" 
                item-prop="thumbnail"
                class="img-thumbnail" 
                alt="<?=$photo['title']?>" />
              </a>
            </figure>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</article>
<?php endwhile; ?>







<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>