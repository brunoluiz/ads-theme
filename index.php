<?php 
$simfolio = new Simfolio();

if( is_tax() ) {
  $term = $wp_query->queried_object;
  $args = array(
    'post_type' => 'simfolio-project',
    'orderby' => 'rand',
    'tax_query' => array(
      array (
        'taxonomy' => 'skill',
        'field' => 'slug',
        'terms' => $term->slug
      )
    )
  );
  $loop = $simfolio->get_projects($args); 
}
else {
  $loop = $simfolio->get_projects(array( 'orderby'=>'rand' )); 
}
?>

<div id="freewall-layout">
<?php if (!$loop->have_posts()) : ?>
  <div class="nothing-found">
    <i class="fa fa-exclamation-circle"></i> <?php _e('Nenhum projeto encontrado', 'sage'); ?>
  </div>
<?php endif; ?>

  <div id="freewall">
  <?php while ( $loop->have_posts() ) : $loop->the_post();
    get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format());
  endwhile; ?>

  </div>
</div>