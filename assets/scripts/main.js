/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        var wall = new Freewall("#freewall");
        var args = {
          selector: '.item',
          gutterX: 1,
          gutterY: 1,
          cellW: 200,
          cellH: 200,
          // animate: true,
          onResize: function() {
            var width = jQuery(window).width();
            var height = 10000;
            $("#freewall-layout").width = width+"px";
            wall.refresh(width, height);        
            $("#freewall-layout").height = jQuery(".item").last().height() + parseInt(jQuery(".item").last().css("top")) + "px";
          }
        };
        wall.reset(args);

        var width = jQuery(window).width();
        var height = 10000;
        $("#freewall-layout").width = width+"px";
        wall.fitZone(width, height);
        $("#freewall-layout").height = jQuery(".item").last().height() + parseInt(jQuery(".item").last().css("top")) + "px";
      }
    },
    // Home page
    'home': {
      init: function() {
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    'single': {
      init: function() {
        // $('.img-thumbnail').click(function(e) {
        //   $('#project-photo > .project-photo-viewer').attr('href',$(this).attr('data-src-full'));
        //   $('#project-photo > img').attr('src', $(this).attr('data-src'));
        //   $('#project-photo > figcaption > .project-photo-title').html($(this).attr('data-title'));
        //   $('#project-photo > figcaption > .project-photo-caption').html($(this).attr('data-caption'));
        //   window.scrollTo(0,0);
        // });

        $('.photoswipe-collection').each( function() {
          var $pic     = $(this),
              getItems = function() {
                  var items = [];
                  $pic.find('a').each(function() {
                      var $href   = $(this).attr('href'),
                          $width  = $(this).attr('data-width'),
                          $height = $(this).attr('data-height'),
                          $title  = $(this).attr('alt');
                      
                      var item = {
                          src   : $href,
                          w     : $width,
                          h     : $height,
                          title : $title
                      };

       
                      items.push(item);
                  });
                  return items;
              };  
       
          var items = getItems();
          console.log(items);

          var $pswp = $('.pswp')[0];
          $pic.on('click', 'figure', function(event) {
              event.preventDefault();
               
              var $index = $(this).index();
              var options = {
                  index: $index,
                  // bgOpacity: 0.7,
                  showHideOpacity: true
              };
               
              // Initialize PhotoSwipe
              var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
              lightBox.init();
          });
        });
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
