<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div id="page">
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
      </div>
    </div>
  </div>
</div>