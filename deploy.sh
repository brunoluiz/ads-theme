#!/bin/bash

original_path=$PWD
path="/home/storage/9/e1/60/buscapopular/public_html/adsstudio/wp-content/themes/ads"

# Build dist files
gulp build

# Compress the directory ads
rm build.tgz
tar --exclude="./bower_components" --exclude="./node_modules" --exclude="./.git" -zcf build.tgz .

# Send it to server
scp build.tgz buscapopular@buscapopular.com.br:$path

# Uncompress data
ssh buscapopular@buscapopular.com.br "cd $path; tar -xf build.tgz; exit;"

echo 'finished!'